﻿Feature: BBC1 HaveYourSay
	In order to share the news with BBC
	As a User
	I want to be able to fulfill HaveYourSay form
	and send it to BBC

@criticalpath
Scenario: The User fulfills the HaveYourSay Form with "I accept the Terms of Service" checkbox left unchecked
	Given the User goes to the HomePage
	When the User follows from HomePage to YourCoronavirusStoriesHowToShareWithBBCNewsPage
	And the User fulfills the Form and submits it to the server with default data except changing next values
		| FieldMark    | Data      |
		| I accept the | Unchecked |
	Then Warning Messages are displayed
		| FieldMark                     | WarningMessage   |
		| I accept the Terms of Service | must be accepted |
	And the Current URL is the YourCoronavirusStoriesHowToShareWithBBCNewsPage URL

@criticalpath
Scenario: The User fulfills the HaveYourSay Form with Incorrect Email
	Given the User goes to the HomePage
	When the User follows from HomePage to YourCoronavirusStoriesHowToShareWithBBCNewsPage
	And the User fulfills the Form and submits it to the server with default data except changing next values
		| FieldMark     | Data      |
		| Email address | TestEmail |
	Then Warning Messages are displayed
		| FieldMark     | WarningMessage           |
		| Email address | Email address is invalid |
	And the Current URL is the YourCoronavirusStoriesHowToShareWithBBCNewsPage URL

@criticalpath
Scenario: The User fulfills the HaveYourSay Form with "I am over 16 years old" checkbox left unchecked
	Given the User goes to the HomePage
	When the User follows from HomePage to YourCoronavirusStoriesHowToShareWithBBCNewsPage
	And the User fulfills the Form and submits it to the server with default data except changing next values
		| FieldMark              | Data      |
		| I am over 16 years old | Unchecked |
	Then Warning Messages are displayed
		| FieldMark              | WarningMessage   |
		| I am over 16 years old | must be accepted |
	And the Current URL is the YourCoronavirusStoriesHowToShareWithBBCNewsPage URL