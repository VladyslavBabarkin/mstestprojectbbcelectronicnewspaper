﻿Feature: BBC2 Scores of Football Matches
	In order to follow the results of Games and Championships of Football Matches 
	As a User (and a Footballfan)
	I want to be able to see the Scores of Football Matches

@criticalpath
Scenario Outline: Check the Scores of Football Matches
	Given the User goes to the HomePage
	When the User follows from HomePage to ScoresAndFixturesPage
	And the User enters <Championship> search request into the search field and conducts search
	And the User picks the Month <Month> and the Year <Year> from date picker timeline
	Then on the ScoresAndFixturesPage are Displayed for HomeTeam <Home Team> the Score <Home Team Score> and the Score <Away Team Score> for AwayTeam <Away Team>
	And the User can follow from the ScoresAndFixturesPage to FootballMatchDetailsPage of HomeTeam <Home Team> vs AwayTeam <Away Team>
	And on the FootballMatchDetailsPage are Displayed for HomeTeam <Home Team> the Score <Home Team Score> and the Score <Away Team Score> for AwayTeam <Away Team>

	Examples:
		| Championship          | Month | Year | Home Team   | Home Team Score | Away Team Score | Away Team      |
		| Scottish Championship | JAN   | 2020 | Dunfermline | 0               | 1               | Ayr United     |
		| FA Cup                | AUG   | 2020 | Arsenal     | 2               | 1               | Chelsea        |
		| England Football Team | SEP   | 2019 | England     | 4               | 0               | Bulgaria       |
		| Champions League      | FEB   | 2020 | Napoli      | 1               | 1               | Barcelona      |
		| National League       | OCT   | 2019 | Yeovil Town | 1               | 2               | Harrogate Town |