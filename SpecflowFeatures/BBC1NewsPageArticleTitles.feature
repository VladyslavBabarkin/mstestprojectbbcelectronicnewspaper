﻿Feature: BBC1 NewsPageArticlesTitles
	In order to find out the News of the Day
	As a User
	I want to read Titles of Articles on the News Page

@criticalpath
Scenario: Check Name Of Headline Article on NewsPage
	Given the User goes to the HomePage
	When the User follows from HomePage to NewsPage
	Then the User sees the Top Headline Article with Title 'HSBC moved scam millions, big banking leak shows'

@criticalpath
Scenario: Check Titles of Secondary Article on NewsPage
	Given the User goes to the HomePage
	When the User follows from HomePage to NewsPage
	Then the User sees Secondary Articles with Titles
		| Title                                              |
		| Judge blocks US attempts to ban China's WeChat     |
		| Sarcophagi buried for 2,500 years exhumed in Egypt |
		| Flypast marks Battle of Britain anniversary        |
		| Runner gives up medal to rival who went wrong way  |