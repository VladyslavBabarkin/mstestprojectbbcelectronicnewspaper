Feature: BBC1 SearchPageResults
	In order to search Information about Specified Topics of Interest
	As a User
	I want to be able to make Search with Specified Keywords
	and get data about Search Results of My Search Request

@criticalpath
Scenario: Check the name of the first article on the SearchPage against a specified value taken from the NewsPage
	Given the User goes to the HomePage
	When the User follows from HomePage to NewsPage
	And the User gets the text of the Category Link of the Headline Article as search_request
	And the User enters the search_request in the Search Bar
	And the User clicks the Search Button
	Then the Current URL is the SearchResultsPage URL
	And  the <Nth> Article has Title <Title>

	Examples:
		| Nth | Title                       |
		| 0   | Weather forecast for the UK |