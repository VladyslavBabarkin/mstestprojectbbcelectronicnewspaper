﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;


namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class NewsPage : BasePage
    {

       
        

        [FindsBy(How = How.XPath, Using = "//h3[contains(@class, 'promo')][contains(@class, 'heading') or contains(@class, 'title')]")]
        private IList<IWebElement> promoHeadingsOrTitlesH3List;



        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'secondary-item')][contains(@class, '1/5')]//h3")]
        private IList<IWebElement> secondaryArticlesTitlesH3List;
        public IList<IWebElement> SecondaryArticlesTitlesH3List { get { return secondaryArticlesTitlesH3List; } }


        [FindsBy(How = How.XPath, Using = "//div[@data-entityid='container-top-stories#1']//a[contains(@aria-label,'From')]//span")]
        private IWebElement categoryLinkOfHeadlineArticleSpan;
        public IWebElement CategoryLinkOfHeadlineArticleSpan { get { return categoryLinkOfHeadlineArticleSpan; } }


        [FindsBy(How = How.XPath, Using = "//input[@id='orb-search-q']")]
        private IWebElement searchFieldInput;
        public IWebElement SearchFieldInput { get { return searchFieldInput; } }


        [FindsBy(How = How.XPath, Using = "//button[@id='orb-search-button']")]
        private IWebElement searchButton;
        public IWebElement SearchButton { get { return searchButton; } }


        [FindsBy(How = How.XPath, Using = "//nav[contains(@class,'nav__wide')]//ul//li//a[@class='nw-o-link']/span")]
        private IList<IWebElement> newsCategoriesNavigationTitlesSpanList;
        public IList<IWebElement> NewsCategoriesNavigationTitlesSpanList { get { return newsCategoriesNavigationTitlesSpanList; } }


        public String HeadlineArticleTitle { get { return promoHeadingsOrTitlesH3List[0].Text; } }


        public NewsPage(IWebDriver driver) : base(driver) { }  

        public IWebElement GetNavigationNewsCategorySpanByTitle(String CategoryTitle)
        {
            foreach (IWebElement span in newsCategoriesNavigationTitlesSpanList) { if (span.Text == CategoryTitle) { return span; } }
            return null;
        }


        public void ClickOnNavigationNewsCategoryByTitle(String categoryTitle)
        {
            GetNavigationNewsCategorySpanByTitle(categoryTitle).Click();
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(new System.TimeSpan(0, 0, 30));
        }


        public List<String> GetSecondaryArticlesTitlesStringList()
        {
            List<String> secondaryArticlesTitlesStringList = new List<string>();
            foreach (IWebElement h3 in SecondaryArticlesTitlesH3List) { if (h3.Displayed) { secondaryArticlesTitlesStringList.Add(h3.Text); } }
            return secondaryArticlesTitlesStringList;
        }


        public void EnterSearchRequestIntoTheSearchField(string searchRequest) { SearchFieldInput.SendKeys(searchRequest); }
        public void ClickOnSearchButton() { SearchButton.Click(); }


    }
}