﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{


    public class HomePage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//div[@id='orb-nav-links']//li[contains(@class,'newsdotcom')]")]
        private IWebElement navigationNewsLi;
        public IWebElement NavigationNewsLi { get { return navigationNewsLi; } }


        [FindsBy(How = How.XPath, Using = "//div[@id='orb-nav-links']//ul/li//a")]
        private IList<IWebElement> navigationLinksIListA;
        public IList<IWebElement> NavigationLinksIListA { get { return navigationLinksIListA; } }



        public String HomePageBBCUrl { get { return "https://www.bbc.com"; } }        
        public String NavigationLinkCategoryTitleNews { get { return "News"; } }      
        public String NavigationLinkCategoryTitleSport { get { return "Sport"; } }



        public HomePage(IWebDriver driver) : base(driver) { }
       


        public void ClickOnNavigationLinkPickedByLinkText(string titleToPickNavigationLinkBy)
        {
            foreach (IWebElement a in navigationLinksIListA)
            {
                if (a.Text == titleToPickNavigationLinkBy)
                {
                    a.Click();
                    WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);
                    break;
                }
            }

        }



        public void ClickOnNavigationNewsLi()
        {
            NavigationNewsLi.Click();
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);

        }




    }




}
