﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class FootballMatchDetailsPage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//span[contains(@class, 'team--home')]//span[contains(@class, 'name--home')]")]
        private IWebElement homeTeamNameSpan;
        public IWebElement HomeTeamNameSpan { get { return homeTeamNameSpan; } }


        [FindsBy(How = How.XPath, Using = "//span[contains(@class, 'team--away')]//span[contains(@class, 'name--away')]")]
        private IWebElement awayTeamNameSpan;
        public IWebElement AwayTeamNameSpan { get { return awayTeamNameSpan; } }



        [FindsBy(How = How.XPath, Using = "//span[contains(@class, 'team--home')]//span[contains(@class, 'number--home')]")]
        private IWebElement homeTeamScoreSpan;
        public IWebElement HomeTeamScoreSpan { get { return homeTeamScoreSpan; } }


        [FindsBy(How = How.XPath, Using = "//span[contains(@class, 'team--away')]//span[contains(@class, 'number--away')]")]
        private IWebElement awayTeamScoreSpan;
        public IWebElement AwayTeamScoreSpan { get { return awayTeamScoreSpan; } }


        public FootballMatchDetailsPage(IWebDriver driver) : base(driver) { }



    }
}
