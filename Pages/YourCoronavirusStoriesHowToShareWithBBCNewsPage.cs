﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class YourCoronavirusStoriesHowToShareWithBBCNewsPage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//div[text()=' must be accepted' and preceding-sibling:: label//p[contains(text(),'I am over 16 years old')]]")]
        private IWebElement iAmOver16YearsOldMustBeAcceptedWarningMessageDiv;
        public IWebElement IAmOver16YearsOldMustBeAcceptedWarningMessageDiv { get { return iAmOver16YearsOldMustBeAcceptedWarningMessageDiv; } }


        [FindsBy(How = How.XPath, Using = "//div[text()=' must be accepted' and preceding-sibling:: label//a[contains(text(),'Terms of Service')]]")]
        private IWebElement iAcceptTheTermsOfServiceMustBeAcceptedWarningMessageDiv;
        public IWebElement IAcceptTheTermsOfServiceMustBeAcceptedWarningMessageDiv { get { return iAcceptTheTermsOfServiceMustBeAcceptedWarningMessageDiv; } }


        [FindsBy(How = How.XPath, Using = "//div[text()='Email address is invalid' and preceding-sibling:: input[@aria-label='Email address']]")]
        private IWebElement eMailAddressIsInvalidWarningMessageDiv;
        public IWebElement EMailAddressIsInvalidWarningMessageDiv { get { return eMailAddressIsInvalidWarningMessageDiv; } }



        [FindsBy(How = How.XPath, Using = "//button[text()='Submit']")]
        private IWebElement submitButton;
        public IWebElement SubmitButton { get { return submitButton; } }



        public String YourCoronavirusStoriesHowToShareWithBBCNewsPageURL { get { return "https://www.bbc.com/news/10725415"; } }


        public YourCoronavirusStoriesHowToShareWithBBCNewsPage(IWebDriver driver) : base(driver) { }



        public void FillForm(Dictionary<string, string> values)
        {



            Dictionary<string, string> FILL_FORM_DATA_SET_DEFAULT_VALUES = new Dictionary<string, string>()
            {

                ["Tell us your story. "] = "TestInputText",
                ["Name"] = "TestName",
                ["Email address"] = "TestEmail@email.com",
                ["Contact number "] = "TestContactNumber",
                ["Location "] = "TestInputText",
                ["Please don"] = "Checked",
                ["I am over 16 years old"] = "Checked",
                ["I accept the"] = "Checked"
            };

            foreach (KeyValuePair<string, string> kv in values)
            {
                FILL_FORM_DATA_SET_DEFAULT_VALUES[kv.Key] = kv.Value;
            }


            foreach (KeyValuePair<string, string> kv in FILL_FORM_DATA_SET_DEFAULT_VALUES)
            {

                IWebElement WebElement = Driver.FindElement(By.XPath("//*[contains(@aria-label,'" + kv.Key + "') or (following-sibling::span//p[contains(text(),'" + kv.Key + "')])]"));
                         

                if (WebElement.TagName == "textarea") { WebElement.SendKeys(kv.Value); }
                else if (WebElement.TagName == "input")
                {

                    if (WebElement.GetAttribute("type") == "text") { WebElement.SendKeys(kv.Value); }

                    else if (WebElement.GetAttribute("type") == "checkbox")
                    {

                        if (kv.Value == "Checked" && !WebElement.Selected) { WebElement.Click(); }
                        else if (kv.Value == "Unchecked" && WebElement.Selected) { WebElement.Click(); }

                    }

                }
               
            }
           

            SubmitButton.Click();           
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);
           
        }

    }


}








