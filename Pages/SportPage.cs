﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;


namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class SportPage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//div[@aria-label='Primary']//a[@data-stat-title='Football']")]
        private IWebElement footballMenuItemA;
        public IWebElement FootballMenuItemA { get { return footballMenuItemA; } }



        [FindsBy(How = How.XPath, Using = "//a[@data-stat-title='Scores & Fixtures']")]
        private IWebElement footballScoresAndFixturesNavigationA;
        public IWebElement FootballScoresAndFixturesNavigationA { get { return footballScoresAndFixturesNavigationA; } }


        public SportPage(IWebDriver driver) : base(driver) { }


        public void GoFromSportPageToFootballScoresAndFixturesPage()
        {
            FootballMenuItemA.Click();
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);
            FootballScoresAndFixturesNavigationA.Click();
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);
        }




    }
}
