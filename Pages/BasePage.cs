﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class BasePage
    {

  

        [FindsBy(How = How.XPath, Using = "//div[@id='sign_in'][@role='alertdialog']")]
        private IWebElement signInAlertDialog;
        public IWebElement SignInAlertDialog { get { return signInAlertDialog; } }


        [FindsBy(How = How.XPath, Using = " //button[@class='sign_in-exit']")]
        private IWebElement signInAlertDialogExitButton;
        public IWebElement SignInAlertDialogExitButton { get { return signInAlertDialogExitButton; } }


        private static IWebDriver driver;
        public static IWebDriver Driver { get { return driver; } }


        private TimeSpan timeSpan30Sec = new TimeSpan(0, 0, 30);
        public TimeSpan TimeSpan30Sec { get { return timeSpan30Sec; } }


        public string CurrentURL => Driver.Url;



        public BasePage(IWebDriver iWebDriver)
        {
            driver = iWebDriver;
            PageFactory.InitElements(driver, this);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);


        }

        public void WaitForPageLoadComplete(TimeSpan timeToWait) { new WebDriverWait(driver, timeToWait).Until(webDriver => ((IJavaScriptExecutor)webDriver).ExecuteScript("return document.readyState").Equals("complete")); }


        public void WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan timeToWait)
        {
            new WebDriverWait(driver, timeToWait).Until(webDriver => ((IJavaScriptExecutor)webDriver).ExecuteScript("return document.readyState").Equals("complete"));
            ExitAndCloseSignInAlertDialogIfExists();
        }



        public void MoveToWebElement(IWebElement WebElement)
        {
            Actions action = new Actions(Driver);
            action.MoveToElement(WebElement).Build().Perform();
        }


        public bool IfSignInAlertDialogExists()
        {
            try { SignInAlertDialog.GetAttribute("innerHTML"); return true; }
            catch (NoSuchElementException exception) { return false; }
        }


        public void ClickOnSignInAlertDialogExitButton() { SignInAlertDialogExitButton.Click(); }




        public void ExitAndCloseSignInAlertDialogIfExists()
        {
            if (IfSignInAlertDialogExists()) { ClickOnSignInAlertDialogExitButton(); }
        }



    }


}

