﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class YourCoronavirusStoriesHaveYourSayPage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//a[child::h3[text()='How to share with BBC News']]")]
        private IWebElement howToShareWithBBCNewsA;
        public IWebElement HowToShareWithBBCNewsA { get { return howToShareWithBBCNewsA; } }

        public YourCoronavirusStoriesHaveYourSayPage(IWebDriver driver) : base(driver) { }

        public void ClickOnHowToShareWithBBCNewsA() { howToShareWithBBCNewsA.Click(); WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec); }



    }
}
