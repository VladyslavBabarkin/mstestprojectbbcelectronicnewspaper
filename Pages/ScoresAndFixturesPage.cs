﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class ScoresAndFixturesPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//input[@name='search']")]
        private IWebElement searchScoresAndFixturesInput;
        public IWebElement SearchScoresAndFixturesInput { get { return searchScoresAndFixturesInput; } }


        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'date-picker-timeline__group')]//li")]
        private IList<IWebElement> datePickerTimeLineListLiIList;
        public IList<IWebElement> DatePickerTimeLineLiIList { get { return datePickerTimeLineListLiIList; } }


        [FindsBy(How = How.XPath, Using = "//a[@class = 'sp-c-search__result-item']")]
        private IList<IWebElement> searchResultItemsAIList;
        public IList<IWebElement> SearchResultItemsAIList { get { return searchResultItemsAIList; } }


        [FindsBy(How = How.XPath, Using = " //li[contains(@class,'list-ui__item')]")]
        private IList<IWebElement> teamsAndScoresLiIList;
        public IList<IWebElement> TeamsAndScoresLiIList { get { return teamsAndScoresLiIList; } }


        public ScoresAndFixturesPage(IWebDriver driver) : base(driver) { }


        public void SearchScoresAndFixturesByEnteredText(string searchRequest)
        {
            EnterSearchRequestIntoTheSearchScoresAndFixturesSearchField(searchRequest);
            PickUpSearchResult(searchRequest);
        }


        public void EnterSearchRequestIntoTheSearchScoresAndFixturesSearchField(string searchRequest) { SearchScoresAndFixturesInput.SendKeys(searchRequest); }

        public void PickUpSearchResult(string searchRequest)
        {
            foreach (IWebElement a in SearchResultItemsAIList) { if (String.Compare(a.Text, searchRequest) == 0) { a.Click(); break; } }  
        }

        public void PickUpDateFromDatePickerTimeLineLiIList(string month, string year)
        {
            IList<IWebElement> searchedDateLinkA = Driver.FindElements(By.XPath($"//a[child::span[contains(text(), '{month}')] and child::span[contains(text(), '{year}')]]"));
            if (searchedDateLinkA.Count == 1) { searchedDateLinkA[0].Click(); }   
        }


    }
}
