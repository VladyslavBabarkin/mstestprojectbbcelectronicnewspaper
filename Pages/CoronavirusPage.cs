﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class CoronavirusPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//nav[contains(@class,'wide-secondary')]//span[text()='Your Coronavirus Stories']")]
        private IWebElement secondaryNavYourCoronavirusStoriesSpan;

        public CoronavirusPage(IWebDriver driver) : base(driver) { }

        public void ClickOnSecondaryNavYourCoronavirusStoriesSpan()
        {
            secondaryNavYourCoronavirusStoriesSpan.Click();
            WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);
        }




    }
}
