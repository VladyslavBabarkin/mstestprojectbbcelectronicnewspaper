﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;

namespace MSTestProjectBBCElectronicNewsPaper.Pages
{
    public class SearchResultsPage : BasePage
    {

        [FindsBy(How = How.XPath, Using = "//ul[@role='list' and contains(@class,'Stack')]//li//a//span")]
        private IList<IWebElement> searchResultULPromoHeadlineSpanList;
        public IList<IWebElement> SearchResultULPromoHeadlineSpanList { get { return searchResultULPromoHeadlineSpanList; } }
               
        public String SearchResultsPageURL { get { return "https://www.bbc.co.uk/search?q="; } }

        public SearchResultsPage(IWebDriver driver) : base(driver) { }      

        public String GetNthPromoHeadlineTitle(int n) { return SearchResultULPromoHeadlineSpanList.ElementAt(n).Text; }

    }
}

