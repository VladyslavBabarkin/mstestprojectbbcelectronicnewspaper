﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]
    public sealed class NewsPageSteps : BaseSteps
    {


        private readonly ScenarioContext _scenarioContext;

        public NewsPageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Then(@"the User sees the Top Headline Article with Title '(.*)'")]

        public void ThenTheUserSeesTopHeadlineArticleTitleCalledAs(string headlineArticleTitle)
        {
            Assert.AreEqual(0, String.Compare(headlineArticleTitle, NewsPage.HeadlineArticleTitle));
        }



        [Then(@"the User sees Secondary Articles with Titles")]

        public void ThenTheUserSeesSecondaryArticlesWithTitles(Table table)
        {
            CollectionAssert.AreEqual(table.Rows.Select(s => s[0]).ToList(), NewsPage.GetSecondaryArticlesTitlesStringList(), "Error");
        }


        [When(@"the User gets the text of the Category Link of the Headline Article as search_request")]
        public void WhenTheUserGetsTheTextOfTheCategoryLinkOfTheHeadlineArticle()
        {
            ScenarioContext.Current.Add("search_request", NewsPage.CategoryLinkOfHeadlineArticleSpan.Text);
        }


        [When(@"the User enters the search_request in the Search Bar")]
        public void WhenTheUserEntersTheSearchRequestInTheSearchBar()
        {
            NewsPage.EnterSearchRequestIntoTheSearchField(ScenarioContext.Current.Get<String>("search_request"));
        }

        [When(@"the User clicks the Search Button")]
        public void TheUserClicksTheSearchButton()
        {
            NewsPage.ClickOnSearchButton();
        }


    }
}
