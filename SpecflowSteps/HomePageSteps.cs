﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]

    public sealed class HomePageSteps : BaseSteps
    {

        private readonly ScenarioContext _scenarioContext;
        private String navigationNewsCategoryCoronavirusTitle = "Coronavirus";


        public HomePageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Given("the User goes to the HomePage")]
        public void GivenTheUserGoesToTheHomePage()
        {
            Driver.Navigate().GoToUrl(HomePage.HomePageBBCUrl);
            BasePage.WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);

        }



        [When("the User follows from HomePage to NewsPage")]
        public void WhenTheUserFollowsFromHomePageToNewsPage()
        {

            HomePage.ClickOnNavigationNewsLi();
        }



        [When("the User follows from HomePage to YourCoronavirusStoriesHowToShareWithBBCNewsPage")]
        public void WhenTheUserFollowsFromHomePageToYourCoronavirusStoriesHowToShareWithBBCNewsPage()
        {                       
            WhenTheUserFollowsFromHomePageToNewsPage();
            NewsPage.ClickOnNavigationNewsCategoryByTitle(navigationNewsCategoryCoronavirusTitle);
            CoronavirusPage.ClickOnSecondaryNavYourCoronavirusStoriesSpan();
            YourCoronavirusStoriesHaveYourSayPage.ClickOnHowToShareWithBBCNewsA();           
        }



        [When("the User follows from HomePage to ScoresAndFixturesPage")]
        public void GivenTheUserFollowsFromHomePageToScoresAndFixturesPage()
        {
            HomePage.ClickOnNavigationLinkPickedByLinkText(HomePage.NavigationLinkCategoryTitleSport);
            SportPage.GoFromSportPageToFootballScoresAndFixturesPage();
        }





    }

}
