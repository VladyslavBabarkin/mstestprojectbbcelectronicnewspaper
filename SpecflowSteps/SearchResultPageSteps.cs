﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]

    public sealed class SearchResultPageSteps : BaseSteps
    {


        private readonly ScenarioContext _scenarioContext;

        public SearchResultPageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Then(@"the Current URL is the SearchResultsPage URL")]
        public void ThenTheCurrentURLIsTheSearchResultsPageURL()
        {
            Assert.IsTrue(BasePage.CurrentURL.Contains(SearchResultsPage.SearchResultsPageURL));
        }


        [Then(@"the (.*) Article has Title (.*)")]
        public void ThenTheNthArticleHasTitle(int n, string expectedTitle)
        {
            Assert.AreEqual(expectedTitle, SearchResultsPage.GetNthPromoHeadlineTitle(n));
        }


    }


}
