﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]
    public sealed class YourCoronavirusStoriesHowToShareWithBBCNewsPageSteps : BaseSteps
    {


        private readonly ScenarioContext _scenarioContext;

        public YourCoronavirusStoriesHowToShareWithBBCNewsPageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }



        [When(@"the User fulfills the Form and submits it to the server with default data except changing next values")]
        public void WhenTheUserFulfillsTheFormAndSubmitsItToTheServerWithDefaultDataExceptChangingNextValues(Table table)
        {
            Dictionary<string, string> testData = new Dictionary<string, string>();

            if (table != null)
            {

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    testData.Add(table.Rows[i][0].ToString(), table.Rows[i][1].ToString());
                }

            }

            YourCoronavirusStoriesHowToShareWithBBCNewsPage.FillForm(testData);


        }




        [Then(@"Warning Messages are displayed")]
        public void WarningMessageIsDisplayed(Table table)
        {

            if (table != null)
            {

                for (int i = 0; i < table.Rows.Count; i++)
                {


                    if (table.Rows[i]["FieldMark"] == "I accept the Terms of Service")
                    {

                        Assert.IsTrue(YourCoronavirusStoriesHowToShareWithBBCNewsPage.IAcceptTheTermsOfServiceMustBeAcceptedWarningMessageDiv.Displayed);
                        Assert.AreEqual(0, String.Compare(table.Rows[i]["WarningMessage"], YourCoronavirusStoriesHowToShareWithBBCNewsPage.IAcceptTheTermsOfServiceMustBeAcceptedWarningMessageDiv.Text));
                    }
                    else if (table.Rows[i]["FieldMark"] == "Email address")
                    {
                        Assert.IsTrue(YourCoronavirusStoriesHowToShareWithBBCNewsPage.EMailAddressIsInvalidWarningMessageDiv.Displayed);
                        Assert.AreEqual(0, String.Compare(table.Rows[i]["WarningMessage"], YourCoronavirusStoriesHowToShareWithBBCNewsPage.EMailAddressIsInvalidWarningMessageDiv.Text));
                    }

                    else if (table.Rows[i]["FieldMark"] == "I am over 16 years old")
                    {
                        Assert.IsTrue(YourCoronavirusStoriesHowToShareWithBBCNewsPage.IAmOver16YearsOldMustBeAcceptedWarningMessageDiv.Displayed);
                        Assert.AreEqual(0, String.Compare(table.Rows[i]["WarningMessage"], YourCoronavirusStoriesHowToShareWithBBCNewsPage.IAmOver16YearsOldMustBeAcceptedWarningMessageDiv.Text));

                    }

                }

            }

        }




        [Then(@"the Current URL is the YourCoronavirusStoriesHowToShareWithBBCNewsPage URL")]
        public void TheCurrentURLIsTheYourCoronavirusStoriesHowToShareWithBBCNewsPageURL()
        {
            Assert.AreEqual(0, String.Compare(BasePage.CurrentURL, YourCoronavirusStoriesHowToShareWithBBCNewsPage.YourCoronavirusStoriesHowToShareWithBBCNewsPageURL));
        }






    }
}
