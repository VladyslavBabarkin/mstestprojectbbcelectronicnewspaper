﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;
using MSTestProjectBBCElectronicNewsPaper.Pages;
using OpenQA.Selenium.Chrome;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]
    public class BaseSteps
    {


        private static IWebDriver driver;
        public static IWebDriver Driver { get { if (driver == null) { driver = new ChromeDriver(); } return driver; } }


        private static BasePage basePage;
        public static BasePage BasePage { get { if (basePage == null) { basePage = new BasePage(Driver); } return basePage; } }


        private static HomePage homePage;
        public static HomePage HomePage { get { if (homePage == null) { homePage = new HomePage(Driver); } return homePage; } }


        private static NewsPage newsPage;
        public static NewsPage NewsPage { get { if (newsPage == null) { newsPage = new NewsPage(Driver); } return newsPage; } }


        private static SearchResultsPage searchResultsPage;
        public static SearchResultsPage SearchResultsPage { get { if (searchResultsPage == null) { searchResultsPage = new SearchResultsPage(Driver); } return searchResultsPage; } }

        private static CoronavirusPage coronavirusPage;
        public static CoronavirusPage CoronavirusPage { get { if (coronavirusPage == null) { coronavirusPage = new CoronavirusPage(Driver); } return coronavirusPage; } }


        private static YourCoronavirusStoriesHaveYourSayPage yourCoronavirusStoriesHaveYourSayPage;
        public static YourCoronavirusStoriesHaveYourSayPage YourCoronavirusStoriesHaveYourSayPage { get { if (yourCoronavirusStoriesHaveYourSayPage == null) { yourCoronavirusStoriesHaveYourSayPage = new YourCoronavirusStoriesHaveYourSayPage(Driver); } return yourCoronavirusStoriesHaveYourSayPage; } }


        private static YourCoronavirusStoriesHowToShareWithBBCNewsPage yourCoronavirusStoriesHowToShareWithBBCNewsPage;
        public static YourCoronavirusStoriesHowToShareWithBBCNewsPage YourCoronavirusStoriesHowToShareWithBBCNewsPage { get { if (yourCoronavirusStoriesHowToShareWithBBCNewsPage == null) { yourCoronavirusStoriesHowToShareWithBBCNewsPage = new YourCoronavirusStoriesHowToShareWithBBCNewsPage(Driver); } return yourCoronavirusStoriesHowToShareWithBBCNewsPage; } }


        private static SportPage sportPage;
        public static SportPage SportPage { get { if (sportPage == null) { sportPage = new SportPage(Driver); } return sportPage; } }


        private static ScoresAndFixturesPage scoresAndFixturesPage;
        public static ScoresAndFixturesPage ScoresAndFixturesPage { get { if (scoresAndFixturesPage == null) { scoresAndFixturesPage = new ScoresAndFixturesPage(Driver); } return scoresAndFixturesPage; } }


        private static FootballMatchDetailsPage footballMatchDetailsPage;
        public static FootballMatchDetailsPage FootballMatchDetailsPage { get { if (footballMatchDetailsPage == null) { footballMatchDetailsPage = new FootballMatchDetailsPage(Driver); } return footballMatchDetailsPage; } }



        private TimeSpan timeSpan30Sec = new TimeSpan(0, 0, 30);
        public TimeSpan TimeSpan30Sec { get { return timeSpan30Sec; } }



        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            if (driver == null) { driver = new ChromeDriver(); }
            Driver.Manage().Window.Maximize();
        }


        [AfterTestRun]
        public static void AfterTestRun()
        {
            Driver.Quit();
        }

    }
}
