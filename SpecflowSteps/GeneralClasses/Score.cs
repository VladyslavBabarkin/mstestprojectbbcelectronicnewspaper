﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps.GeneralClasses
{
    public class Score
    {
        private string homeTeamScore = null;
        public string HomeTeamScore { get { return homeTeamScore; } set { homeTeamScore = value; } }

        private string awayTeamScore = null;
        public string AwayTeamScore { get { return awayTeamScore; } set { awayTeamScore = value; } }

        public Score() { }

        public Score(string homeTeamScore, string awayTeamScore)
        {
            this.HomeTeamScore = homeTeamScore;
            this.AwayTeamScore = awayTeamScore;

        }

    }
}
