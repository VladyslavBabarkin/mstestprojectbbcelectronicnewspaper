﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestProjectBBCElectronicNewsPaper.SpecflowSteps.GeneralClasses;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]
    public sealed class ScoresAndFixturesPageSteps : BaseSteps
    {


        private readonly ScenarioContext _scenarioContext;

        public ScoresAndFixturesPageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [When(@"the User enters (.*) search request into the search field and conducts search")]
        public void WhenTheUserEnterSearchRequestIntoTheSearchFieldAndConductsSearch(string searchRequest) { ScoresAndFixturesPage.SearchScoresAndFixturesByEnteredText(searchRequest); }

        [When(@"the User picks the Month (.*) and the Year (.*) from date picker timeline")]
        public void WhenTheUserPicksTheYearAndMonthFromDatePickerTimeline(string month, string year) { ScoresAndFixturesPage.PickUpDateFromDatePickerTimeLineLiIList(month, year); }
        


        [Then(@"on the ScoresAndFixturesPage are Displayed for HomeTeam (.*) the Score (.*) and the Score (.*) for AwayTeam (.*)")]
        public void ThenOnTheScoresAndFixturesPageAreDisplayedForHomeTeamTheScoreAndTheScoreForAwayTeam(string homeTeamNameChecked, string homeTeamScoreChecked, string awayTeamScoreChecked, string awayTeamNameChecked)
        {
            ScoreBoard scoreBoard = new ScoreBoard();
            Score score = scoreBoard.GetScore(homeTeamNameChecked, awayTeamNameChecked);
            Assert.AreEqual(0, String.Compare(homeTeamScoreChecked, score.HomeTeamScore));
            Assert.AreEqual(0, String.Compare(awayTeamScoreChecked, score.AwayTeamScore));
        }


        [Then(@"the User can follow from the ScoresAndFixturesPage to FootballMatchDetailsPage of HomeTeam (.*) vs AwayTeam (.*)")]
        public void ThenTheUserCanFollowFromTheScoresAndFixturesPageToFootballMatchDetailsPage(string homeTeamNameChecked, string awayTeamNameChecked)
        {


            IWebElement linkToMatchOfCheckedTeams = Driver.FindElement(By.XPath($"//a[descendant::span[contains(@class,'team--home') and descendant::span[contains(@class, 'team-name') and text()='{homeTeamNameChecked}']] and descendant::span[contains(@class,'team--away') and descendant::span[contains(@class, 'team-name') and text()='{awayTeamNameChecked}']]]"));
            linkToMatchOfCheckedTeams.Click();
            BasePage.WaitForPageLoadCompleteAndExitAndCloseSignInAlertDialogIfExists(TimeSpan30Sec);


        }



        public class ScoreBoard
        {

            public Score GetScore(string team1, string team2)
            {
                Score DisplayedScore = new Score();

                IWebElement MatchInfoLi = Driver.FindElement(By.XPath($"//li[descendant::span[contains(@class,'team--home')]//span[contains(@class,'full-team-name') and contains(text(),'{team1}')] and descendant::span[contains(@class,'team--away')]//span[contains(@class,'full-team-name') and contains(text(),'{team2}')]]"));

                DisplayedScore.HomeTeamScore = MatchInfoLi.FindElement(By.XPath(".//span[contains(@class,'number--home')]")).Text;
                DisplayedScore.AwayTeamScore = MatchInfoLi.FindElement(By.XPath(".//span[contains(@class,'number--away')]")).Text;

                return DisplayedScore;
            }


        }

    }
}
