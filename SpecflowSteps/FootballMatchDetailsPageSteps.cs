﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTestProjectBBCElectronicNewsPaper.SpecflowSteps
{
    [Binding]
    public sealed class FootballMatchDetailsPageSteps : BaseSteps
    {


        private readonly ScenarioContext _scenarioContext;

        public FootballMatchDetailsPageSteps(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }


        [Then(@"on the FootballMatchDetailsPage are Displayed for HomeTeam (.*) the Score (.*) and the Score (.*) for AwayTeam (.*)")]
        public void ThenOnTheFootballMatchDetailsPageAreDisplayedForHomeTeamTheScoreAndTheScoreForAwayTeam(string homeTeamNameChecked, string homeTeamScoreChecked, string awayTeamScoreChecked, string awayTeamNameChecked)
        {

            if (String.Compare(homeTeamNameChecked, FootballMatchDetailsPage.HomeTeamNameSpan.Text) == 0 && String.Compare(awayTeamNameChecked, FootballMatchDetailsPage.AwayTeamNameSpan.Text) == 0)
            {
                Assert.AreEqual(0, String.Compare(homeTeamScoreChecked, FootballMatchDetailsPage.HomeTeamScoreSpan.Text));
                Assert.AreEqual(0, String.Compare(awayTeamScoreChecked, FootballMatchDetailsPage.AwayTeamScoreSpan.Text));
            }
            else { Assert.Fail(); }

        }

    }
}
