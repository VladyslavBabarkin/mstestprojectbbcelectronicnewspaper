﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace MSTestProjectBBCElectronicNewsPaper
{
    public class Form
    {
        //Additional  Variations of Generalized Realization of Forms Processing 

        // ------------- 1
        static public void FillFormUsingUniqueAttribute(Dictionary<string, string> values, String findAttribute, IWebDriver driver)
        {
            IWebElement ISubmit = null;

            foreach (KeyValuePair<string, string> kv in values)
            {

                IWebElement WebElement = driver.FindElement(By.XPath("//*[@" + findAttribute + "='" + kv.Key + "']"));


                if (WebElement.TagName == "textarea") { WebElement.SendKeys(kv.Value); }

                else if (WebElement.TagName == "input")
                {

                    if (WebElement.GetAttribute("type") == "text") { WebElement.SendKeys(kv.Value); }

                    else if (WebElement.GetAttribute("type") == "checkbox")
                    {

                        if (kv.Value == "Checked" && !WebElement.Selected) { WebElement.Click(); }
                        else if (kv.Value == "Unchecked" && WebElement.Selected) { WebElement.Click(); }

                    }


                }

                else if (WebElement.TagName == "button")
                {
                    if (kv.Value == "Click") { WebElement.Click(); }
                    else if (kv.Value == "Submit") { ISubmit = WebElement; }
                }


            }

            ISubmit.Click();

        }


        // ------------- 2
        public class FormInputsAmple
        {


            public String FindAttribute;
            public String FindValue;
            public String InputFieldValue;
            public String FindByFullXpath;

            public FormInputsAmple(String FindAttribute, String FindValue, String InputFieldValue, String FindByFullXpath)
            {

                this.FindAttribute = FindAttribute;
                this.FindValue = FindValue;
                this.InputFieldValue = InputFieldValue;
                this.FindByFullXpath = FindByFullXpath;
            }



        }



        public static void FillFormUsingFormIputsAmpleClass(List<FormInputsAmple> formInputs, IWebDriver iWebDriver)
        {

            IWebElement ISubmit = null;

            foreach (FormInputsAmple fInpt in formInputs)
            {

                IWebElement WebElement;
                if (fInpt.FindByFullXpath.ToLower() == "y") { WebElement = iWebDriver.FindElement(By.XPath(fInpt.FindValue)); }
                else { WebElement = iWebDriver.FindElement(By.XPath("//*[@" + fInpt.FindAttribute + "='" + fInpt.FindValue + "']")); }



                if (WebElement.TagName == "textarea") { WebElement.SendKeys(fInpt.InputFieldValue); }

                else if (WebElement.TagName == "input")
                {

                    if (WebElement.GetAttribute("type") == "text") { WebElement.SendKeys(fInpt.InputFieldValue); }

                    else if (WebElement.GetAttribute("type") == "checkbox")
                    {

                        if (fInpt.InputFieldValue == "Checked" && !WebElement.Selected) { WebElement.Click(); }
                        else if (fInpt.InputFieldValue == "Unchecked" && WebElement.Selected) { WebElement.Click(); }

                    }


                }

                else if (WebElement.TagName == "button")
                {
                    if (fInpt.InputFieldValue == "Click") { WebElement.Click(); }
                    else if (fInpt.InputFieldValue == "Submit") { ISubmit = WebElement; }
                }


            }

            ISubmit.Click();

        }



        // ------------- 3
        public class FormInputsLaconic
        {


            public String FindAttribute;
            public String FindValue;
            public String InputFieldValue;


            public FormInputsLaconic(String FindAttribute, String FindValue, String InputFieldValue)
            {

                this.FindAttribute = FindAttribute;
                this.FindValue = FindValue;
                this.InputFieldValue = InputFieldValue;

            }



        }





        public static void FillFormUsingFormIputsLaconicClass(List<FormInputsLaconic> formInputs, IWebDriver iWebDriver)
        {

            IWebElement ISubmit = null;

            foreach (FormInputsLaconic fInpt in formInputs)
            {

                IWebElement WebElement = null;



                try { WebElement = iWebDriver.FindElement(By.XPath("//*[@" + fInpt.FindAttribute + "='" + fInpt.FindValue + "']")); }
                catch (Exception e)
                {
                    try { WebElement = iWebDriver.FindElement(By.XPath(fInpt.FindValue)); } catch (Exception ex) { }
                }




                if (WebElement != null)
                {


                    if (WebElement.TagName == "textarea") { WebElement.SendKeys(fInpt.InputFieldValue); }

                    else if (WebElement.TagName == "input")
                    {

                        if (WebElement.GetAttribute("type") == "text") { WebElement.SendKeys(fInpt.InputFieldValue); }

                        else if (WebElement.GetAttribute("type") == "checkbox")
                        {

                            if (fInpt.InputFieldValue == "Checked" && !WebElement.Selected) { WebElement.Click(); }
                            else if (fInpt.InputFieldValue == "Unchecked" && WebElement.Selected) { WebElement.Click(); }

                        }


                    }

                    else if (WebElement.TagName == "button")
                    {
                        if (fInpt.InputFieldValue == "Click") { WebElement.Click(); }
                        else if (fInpt.InputFieldValue == "Submit") { ISubmit = WebElement; }
                    }


                }

            }

            ISubmit.Click();
        }










        // ------------- 4
        public class FormInputs
        {

            public String InputFieldType;
            public String FindAttribute;
            public String FindValue;
            public String InputFieldValue;
            public Boolean FindByFullXpath = false;

            public FormInputs(String InputFieldType, String FindAttribute, String FindValue, String InputFieldValue, Boolean FindByFullXpath)
            {
                this.InputFieldType = InputFieldType;
                this.FindAttribute = FindAttribute;
                this.FindValue = FindValue;
                this.InputFieldValue = InputFieldValue;
                this.FindByFullXpath = FindByFullXpath;
            }



        }

        public static void FillForm(List<FormInputs> formInputs, IWebDriver driver)
        {
            foreach (FormInputs fInpt in formInputs)
            {

                if (fInpt.InputFieldType == "textarea")
                {


                    if (fInpt.FindByFullXpath) { driver.FindElement(By.XPath(fInpt.FindValue)).SendKeys(fInpt.InputFieldValue); }
                    else { driver.FindElement(By.XPath("//textarea[@" + fInpt.FindAttribute + "=" + "'" + fInpt.FindValue + "']")).SendKeys(fInpt.InputFieldValue); }


                }

                if (fInpt.InputFieldType == "text")
                {


                    if (fInpt.FindByFullXpath) { driver.FindElement(By.XPath(fInpt.FindValue)).SendKeys(fInpt.InputFieldValue); }
                    else { driver.FindElement(By.XPath("//input[@" + fInpt.FindAttribute + "=" + "'" + fInpt.FindValue + "']")).SendKeys(fInpt.InputFieldValue); }


                }



                else if (fInpt.InputFieldType == "checkbox")
                {
                    IWebElement checkbox = null;

                    if (fInpt.FindByFullXpath) { checkbox = driver.FindElement(By.XPath(fInpt.FindValue)); }
                    else { checkbox = driver.FindElement(By.XPath("//checkbox[@" + fInpt.FindAttribute + "=" + "'" + fInpt.FindValue + "']")); }

                    if (fInpt.InputFieldValue == "Checked" && !checkbox.Selected) { checkbox.Click(); }
                    else if (fInpt.InputFieldValue == "NonChecked" && checkbox.Selected) { checkbox.Click(); }                 



                }



            }

        }



















    }

}
